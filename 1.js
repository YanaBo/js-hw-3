//Описать своими словами для чего вообще нужны функции в программировании.
//Фнкции нужны, чтобы повторить участок кода необходимое количество раз.
// С помощью функций мы оптимизируем код и он становиться более читабельным.

// Описать своими словами, зачем в функцию передавать аргумент.
//Аргумент - это локальная переменная, которая доступна внутри функции. При вызове функции, аргументы присваиваются параметрам функции, в порядке,
// в котором они записаны. Аргументы позволяют сделать переменные динамическими.


let num1 = prompt('Enter first number');
let num2 = prompt('Enter second number');

while (Number.isNaN(+num1) || Number.isNaN(+num2)) {
  num1 = +prompt('Enter first number once again', num1);
  num2 = +prompt('Enter second number once again', num2);
}

const operation = prompt('Enter operation');

function calcOperation(num1, num2, operation) {
  let result; 
  switch (operation) {
    case '+':
      result = num1 + num2;
      break;

    case '-':
      result = num1 - num2;
      break;
      
    case '*':
      result = num1 * num2;
      break;

    case '/':
      result = num1 / num2;
      break;

    default: 
      alert('Operation is not valid. Please enter one of +, -, *, /');
  }
  console.log('result: ', result);
}

calcOperation(num1, num2, operation);



